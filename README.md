# Requirements:

 * Python 3 (was created using Python 3.6.5)

# Usage

Note: The usage of this has been tested on Linux. It theoretically should work 
on Windows and Mac as well, but that's up to you to test it out.

1. Simple clone/download the project to your machine
1. Go to the directory in Terminal or CMD
1. Type `python testTable.py`
1. Enjoy!

# Bug Reporting

Please open an issue on the project if you find a bug. That is all. :)

# Examples of output

```
 --                                                 --
|H |                                               |He|
|--|--                               --------------|--|
|Li|  |                             |  |C |N |O |  |  |
|--|--|                             |--|--|--|--|--|--|
|Na|  |                             |  |  |  |  |  |  |
|--|--|-----------------------------|--|--|--|--|--|--|
|  |  |  |  |  |  |  |Fe|  |  |  |  |Ga|Ge|  |  |Br|  |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |Nb|  |  |  |  |  |  |  |  |Sn|  |  |  |  |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |Au|  |  |Pb|  |  |  |Rn|
|--|--|  |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 -----|  |--------------------------------------------
       ||_
       |_ --------------------------------------------
         |  |  |  |Nd|  |  |  |  |  |  |  |Er|  |  |  |
         |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
         |  |  |  |U |  |  |  |Cm|  |  |  |  |  |  |  | 
          --------------------------------------------


Type EXIT to quit.
Otherwise, input: [Element Number] [Element Name] [Element Symbol]
Example (freebie!): 1 Hydrogen H
```
^ Note above that all elements in the table are green as they were
answered correctly


```
 --                                                 --
|H |                                               |He|
|--|--                               --------------|--|
|  |  |                             |  |  |  |  |F |  |
|--|--|                             |--|--|--|--|--|--|
|  |  |                             |Al|  |  |  |Cl|  |
|--|--|-----------------------------|--|--|--|--|--|--|
|  |  |  |  |  |Cr|  |  |  |  |  |  |  |Ge|  |  |  |  |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|--|--|  |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 -----|  |--------------------------------------------
       ||_
       |_ --------------------------------------------
         |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
         |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
         |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | 
          --------------------------------------------

Name given: Germaniumumum | Correct name: Germanium

Type EXIT to quit.
Otherwise, input: [Element Number] [Element Name] [Element Symbol]
Example (freebie!): 1 Hydrogen H
```
^ Note above that the error is in red and `Ge` in the table is red.
