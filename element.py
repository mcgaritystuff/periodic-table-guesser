class Element:
  """An element on the periodic table

    This object simply represents an element on the periodic table. It contains
    information related to whether the user has guessed it and if they got it
    correct or not.

    Attributes:
        name: The name of the element (i.e. Hydrogen)
        symbol: The symbol of the element (i.e. "H")
        number: The element's corresponding number
        row: The row on the periodic table the element is on
        col: The column of the periodic table the element is on
        answered: True/False if the user has tried guessing the element yet
        correct: If the user answered correctly or not
  """

  def __init__(self, name: str, symbol: str, number: int, row: int, col: int):
    """ Initializes Element with the given parameters and answered/correct 
        to false """
    self._name = name
    self._symbol = symbol
    self._number = number
    self._row = row
    self._col = col
 
    self._answered = False
    self._correct = False

  def answer_question(self, name: str, symbol: str) -> str:
    """Answers the element with a name and symbol

    Args:
        name: The name of the element
        symbol: The symbol of the element

    Returns:
        A string containing the wrong responses (or blank if nothing is incorrect)

    """
    
    # Don't allow the user to retry the answer ;)
    if self._answered:
      response = "Answer was already given!"
      return response

    self._answered = True
    self._correct = True
    response = ""

    if name != self._name:
      response += "Name given: "+name+" | Correct name: "+self._name+"\n"
      self._correct = False
    if symbol != self._symbol:
      response += "Symbol given: "+symbol+" | Correct symbol: "+self._symbol+"\n"
      self._correct = False

    return response

  def get_symbol(self) -> str:
    """Get the symbol

    Returns:
        The symbol. If the symbol is 1 character, it returns the symbol + a space
    
    """

    if len(self._symbol) == 2:
      return self._symbol
    else:
      return self._symbol + ' '

  def get_col(self) -> int:
    """Gets the column
    
    Returns:
        The column number
    """
    return self._col

  def get_row(self) -> int:
    """Gets the row
    
    Returns:
        The row number
    """
    return self._row

  def get_answered(self) -> bool:
    """Gets whether the element has been guessed or not
    
    Returns:
        True or false for if it's been guessed
    """
    return self._answered

  def get_correct(self) -> bool:
    """Get whether the user guessed the element correctly or not
    
    Returns:
        True for correct, false for incorrect (default, check get_answered first)
    """
    return self._correct
