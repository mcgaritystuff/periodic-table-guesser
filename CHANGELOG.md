# v1.1.3

 * Fix out of bounds for array length when guessing an element

# v1.1.2

 * Fix typo in Gadolinium

# v1.1.1

 * Fix example and instructions (so user must input #, Name, Symbol in that order)

# v1.1.0

 * Update `README` with "screenshots"
 * Fix bug that caused the program to crash if a string was inserted to the element number (int)
 * Fix bug that allowed users to fix their incorrect answers before

# v1.0.0

 * Initial release
 * Allows for user to do the quiz!
