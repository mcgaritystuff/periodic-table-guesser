#!/usr/bin/python

from element import Element

class colors:
    GREEN = '\033[92m'
    RED   = '\033[91m'
    END = '\033[0m'

periodic_table = """ --                                                 --
|  |                                               |  |
|--|--                               --------------|--|
|  |  |                             |  |  |  |  |  |  |
|--|--|                             |--|--|--|--|--|--|
|  |  |                             |  |  |  |  |  |  |
|--|--|-----------------------------|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|--|--|  |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 -----|  |--------------------------------------------
       ||_
       |_ --------------------------------------------
         |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
         |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
         |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | 
          --------------------------------------------
"""

response_message = ""
all_elements = []

def printTable():  
  """Prints the current periodic table

  Simply prints the current periodic table based on the elements that have been
  guessed by the user. If the user hasn't guessed the element yet, it's left as
  a blank box. If they have, it'll be green for correct and red for incorrect.
   
  """

  periodic_table_rows = periodic_table.split('\n')

  # We have to reverse so the colors don't mess up the string splicing
  for element in reversed(all_elements):
    # Skip elements that haven't been answered yet
    if not element.get_answered():
      continue

    # Grab some information so we know where to draw it and the color to draw in
    table_row = element.get_row() * 2 - 1
    table_col = element.get_col() * 3 - 2
    row_text = periodic_table_rows[table_row]
    color = colors.GREEN if element.get_correct() else colors.RED

    # Replace the part of the table with the element symbol
    periodic_table_rows[table_row] = '%s%s%s'\
        %(row_text[:table_col], \
          color + element.get_symbol() + colors.END, \
          row_text[table_col+2:])

  # Print every row to the periodic table now that it's been updated
  for row in periodic_table_rows:
    print(row)

def parse_elements_file():
  """Parses the elements.txt file

  This assumes that the elements in elements.txt are in order by number.
  It reads every line of the file and then parses it into an Element object.
  Each line in the file should follow the format:
    ElementName ElementSymbol ElementNumber PeriodicTableRow PeriodicTableColumn
   
  """
  elements_file = open('elements.txt', 'r')
  contents = []
  if elements_file.mode == 'r':
    contents = elements_file.readlines()

  for line in contents:
    line_contents = line.split()
    all_elements.append(
      Element(
        line_contents[0], 
        line_contents[1],
        int(line_contents[2]),
        int(line_contents[3]),
        int(line_contents[4])
      )
    )

# Parse the elements file first
parse_elements_file()

# Loop through the application as the user guesses elements on the table.
# The loop will be killed once the user has guessed all the elements, 
# or types EXIT.
while True:
  # Clear the screen
  print("\033[H\033[J")
  printTable()

  # Print out the response message, if applicable, and reset it to blank
  print(response_message)
  response_message = ""

  # Get the user's input and parse it, do stuff, etc. to test the answer
  user_input = input("Type EXIT to quit.\nOtherwise, input: [Element Number] [Element Name] [Element Symbol]\nExample (freebie!): 1 Hydrogen H\n")

  # Test the user trying to exit
  if user_input == "EXIT":
    break

  input_split = user_input.split()
  if len(input_split) != 3:
    response_message = colors.RED + "You need to put exactly all 3 things!" + colors.END
    continue
  elif not input_split[0].isdigit():
    response_message = colors.RED + "First entry must be a number!" + colors.END
    continue
  else:
    element_number = int(input_split[0])
    element_name = input_split[1]
    element_symbol = input_split[2]
    
    if element_number <= 0 or element_number > len(all_elements):
      response_message = colors.RED + "Number must be 1-" + str(len(all_elements)) + colors.END
      continue
    guessing_element = all_elements[element_number - 1]
    response_message = guessing_element.answer_question(element_name, element_symbol)
    if response_message != "":
      response_message = colors.RED + response_message + colors.END
